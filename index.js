const express = require('express')
const cors = require('cors')
const app = express()
const axois = require('axios')
const $ = require('cheerio');

const TWITTER_URL = 'http://twitter.com/'
const DEFAULT_USERNAMES = ['brunoraljic', 'mrFunkyWisdom']

const port = 5002
app.get('/api/track', cors({ origin: '*' }), async (req, res) => {

  const userNames = getUsernames(req.query);

  try {
    const promises = userNames.map(async userName => {
      const response = await axois.get(TWITTER_URL + userName)
      return {
        [userName]: selectFollowers(response.data)
      }
    })
    const results = await Promise.all(promises);

    res
      .status(200)
      .json(mergeResult(results))
  } catch (err) {
    res
      .status(500)
      .json({
        message: err.message,
        config: err.config
      })
  }

});

const getUsernames = params => {
  if (params && params.usenames) {
    return params.usenames.split(',')
  } else {
    return DEFAULT_USERNAMES;
  }
}

///////// select data
const selectFollowers = data => {
  let result = $('a[data-nav="followers"]', data).attr("title");
  return result.replace(/[^0-9]/g, '')
}

const mergeResult = results => {
  let finalResult = {};
  results.map(result => {
    finalResult = Object.assign({}, finalResult, result);
  })
  return finalResult;
}

app.listen(port, () => console.log(`Scraper runs on port ${port}!`))
